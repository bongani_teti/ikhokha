package com.ikhokha.techcheck;

import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;

public class CallableFileReader implements Callable<Map<String, Integer>> {

    File file;

    public CallableFileReader(File file) {
        this.file = file;
    }

    @Override
    public Map<String,Integer> call() {

        CommentAnalyzer commentAnalyzer = new CommentAnalyzer(this.file);
        return commentAnalyzer.analyze();
    }
}
