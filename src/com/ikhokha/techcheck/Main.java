package com.ikhokha.techcheck;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

	public static void main(String[] args) throws ExecutionException, InterruptedException {

		Map<String, Integer> totalResults = new HashMap<>();
		ExecutorService executorService = Executors.newFixedThreadPool(3);

		List<CallableFileReader> list = new ArrayList<>();

		List<Future<Map<String,Integer>>> results = new ArrayList<>();

		File docPath = new File("docs");
		File[] commentFiles = docPath.listFiles((d, n) -> n.endsWith(".txt"));

		assert commentFiles != null;
		for (File commentFile : commentFiles) {
			list.add(new CallableFileReader(commentFile));
		}

		for (CallableFileReader callableFileReader : list) {
			results.add(executorService.submit(callableFileReader));
		}

		for (Future<Map<String,Integer>> result: results) {
			Map<String, Integer> temp = result.get();
			temp.forEach(
					(k, v) -> {
						if (totalResults.containsKey(k)) {
							totalResults.put(k, totalResults.get(k) + v);
						} else {
							totalResults.put(k, v);
						}
					}
			);
		}

		System.out.println("RESULTS\n=======");
		totalResults.forEach((k,v) -> System.out.println(k + " : " + v));

		executorService.shutdown();
	}
}
