package com.ikhokha.techcheck;

import com.ikhokha.techcheck.matcher.AbstractMatcher;
import com.ikhokha.techcheck.matcher.Metric;

import java.util.Arrays;
import java.util.Map;

public class Analyzer {
    public void analyze(String line, Map<String, Integer> resultsMap) {
        Arrays.asList(Metric.values()).forEach(metric -> {
            AbstractMatcher matcher = AbstractMatcher.getInstance(metric);
            matcher.match(line, resultsMap);
        });
    }
}
