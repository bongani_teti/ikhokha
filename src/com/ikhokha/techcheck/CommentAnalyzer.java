package com.ikhokha.techcheck;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CommentAnalyzer {

	private File file;

	public CommentAnalyzer(File file) {
		this.file = file;
	}

	public Map<String, Integer> analyze() {

		Map<String, Integer> resultsMap = new HashMap<>();

		Analyzer analyzer = new Analyzer();

		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			String line;
			while ((line = reader.readLine()) != null) {


				analyzer.analyze(line, resultsMap);

			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Error processing file: " + file.getAbsolutePath());
			e.printStackTrace();
		}

		return resultsMap;

	}

}
