package com.ikhokha.techcheck.matcher;

public class ShorterThanFifteenMatcher extends AbstractMatcher {

    protected ShorterThanFifteenMatcher(String key) {
        this.key = key;
    }

    @Override
    boolean match(String line) {
        return line.length() < 15;
    }
}
