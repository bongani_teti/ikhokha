package com.ikhokha.techcheck.matcher;

import java.util.Map;

public abstract class AbstractMatcher {
    protected String key;

    abstract boolean match(String line);

    public static AbstractMatcher getInstance(Metric metric) {
        switch (metric) {
            case SHORTER_THAN_15:
                return new ShorterThanFifteenMatcher(metric.name());
            case MOVER_MENTIONS:
                return new MoverMentionMatcher(metric.name());
            case SHAKER_MENTIONS:
                return new ShakerMentionMatcher(metric.name());
            case QUESTIONS:
                return new QuestionsMatcher(metric.name());
            case SPAM:
                return new SpamMatcher(metric.name());
        }
        throw new UnsupportedOperationException("Metric not supported");
    }

    /**
     * This method checks if the line matches with the matcher and increment occurrence
     * @param line line to check
     * @param resultsMap results map
     */
    public void match(String line, Map<String, Integer> resultsMap) {
        if (this.match(line)) {
            this.incOccurrence(resultsMap, this.key);
        }
    }

    /**
     * This method increments a counter by 1 for a match type on the countMap. Uninitialized keys will be set to 1
     * @param countMap the map that keeps track of counts
     * @param key the key for the value to increment
     */
    protected void incOccurrence(Map<String, Integer> countMap, String key) {

        countMap.putIfAbsent(key, 0);
        countMap.put(key, countMap.get(key) + 1);
    }
}
