package com.ikhokha.techcheck.matcher;

public class ShakerMentionMatcher extends AbstractMatcher {

    protected ShakerMentionMatcher(String key) {
        this.key = key;
    }

    @Override
    boolean match(String line) {
        return line.contains("Shaker");
    }
}
