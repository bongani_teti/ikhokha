package com.ikhokha.techcheck.matcher;

public class MoverMentionMatcher extends AbstractMatcher {

    protected MoverMentionMatcher(String key) {
        this.key = key;
    }

    @Override
    boolean match(String line) {
        return line.contains("Mover");
    }
}
