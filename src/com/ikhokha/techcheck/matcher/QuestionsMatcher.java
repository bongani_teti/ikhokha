package com.ikhokha.techcheck.matcher;

public class QuestionsMatcher extends AbstractMatcher {

    protected QuestionsMatcher(String key) {
        this.key = key;
    }

    @Override
    boolean match(String line) {
        return line.contains("?");
    }
}
