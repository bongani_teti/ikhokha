package com.ikhokha.techcheck.matcher;

public enum Metric {
    SHORTER_THAN_15,
    MOVER_MENTIONS,
    SHAKER_MENTIONS,
    QUESTIONS,
    SPAM
}
