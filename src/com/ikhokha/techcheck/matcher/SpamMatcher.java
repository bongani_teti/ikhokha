package com.ikhokha.techcheck.matcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpamMatcher extends AbstractMatcher {

    protected SpamMatcher(String key) {
        this.key = key;
    }

    @Override
    boolean match(String line) {
        String regex = "\\b((?:https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:, .;]*[-a-zA-Z0-9+&@#/%=~_|])";
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        return m.find();
    }
}
